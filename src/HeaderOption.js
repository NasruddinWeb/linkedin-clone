import React from 'react';
import './headeroption.css';
import {Avatar} from '@material-ui/core';

function HeaderOption(props) {
    return (
        <div className="headerOption">
            {props.icons && <props.icons className="headerOption-icon"/>}
            {props.avatar && <Avatar className="headerOption-avatar" src={props.avatar}/>}
            <h3 className="headerOption-title">{props.title}</h3>
        </div>
    )
}

export default HeaderOption
