import React from 'react';

import { Counter } from './features/counter/Counter';
import './App.css';
import Header from './Header';
import Sidebar from './Sidebar';
import Feed from './Feed';

function App() {
  return (
    <div className="App">
     {/* HEader */}
     <Header />
     {/* App BOdy */}
     <div className="app-body">
       <Sidebar />
       <Feed />
     </div>
    {/* feed */}

    
    </div>
  );
}

export default App;
