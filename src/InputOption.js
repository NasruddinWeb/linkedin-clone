import React from 'react';
import './inputoptions.css';

function InputOption(props) {
    return (
        <div className="inputOptions">
            <props.icons style={{color:props.color}}/>
            <h4>{props.title}</h4>
        </div>
    )
}

export default InputOption
