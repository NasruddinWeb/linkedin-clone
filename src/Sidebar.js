import { Avatar } from '@material-ui/core';
import React from 'react';

import './sidebar.css';

function Sidebar() {

    const recentItem = (topic) => (
    <div className="sidebar_recentItem">
        <span className="sidebar-hash">#</span>
        <p>{topic}</p>
    </div>
    );



    return (
        <div className="sidebar">
            <div className="sidebar-top">
                <img src="https://images.unsplash.com/photo-1627029260069-dfd6997b4140?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt="" />
                <Avatar className="sidebar-avatar" />
                <h2>Nasruddin</h2>
                <h4>mohammadnasruddin@gmail.com</h4>
            </div>
            <div className="sidebar-stats">
                <div className="sidebar-stat">
                    <p>Who viewed your profile</p>
                    <p className="sidebar-statNumber">10</p>
                </div>
                <div className="sidebar-stat">
                    <p>Who viewed your post</p>
                    <p className="sidebar-statNumber">249</p>
                </div>

            </div>
            <div className="sidebar-bottom">
                <p>Recent</p>
                {recentItem("reactJs")}
                {recentItem("JavaScript")}
                {recentItem("css")}
                {recentItem('HTML')}
            </div>
        </div>
    )
}

export default Sidebar
