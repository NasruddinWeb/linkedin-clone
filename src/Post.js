import { Avatar } from '@material-ui/core';
import React from 'react';
import './post.css';
import InputOption from './InputOption';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import CommentIcon from '@material-ui/icons/Comment';
import SendIcon from '@material-ui/icons/Send';
import ShareIcon from '@material-ui/icons/Share';

function Post({name,description,message,photoUrl, avatarUrl}) {
    return (
        <div className="post">
            <div className="post-header">
            <Avatar src={avatarUrl} />
            <div className="post-info">
                <h2>{name}</h2>
                <p>{description}</p>
            </div>
            </div>
           
            <div className="post-body">
                <p>{message}</p>
            </div>
            <div className="post-button">
                <InputOption icons={ThumbUpAltIcon} title="Like" />
                <InputOption icons={CommentIcon} title="Comment" />
                <InputOption icons={ShareIcon} title="Share" />
                <InputOption icons={SendIcon} title="Send" />
            </div>

        </div>
    )
}

export default Post
