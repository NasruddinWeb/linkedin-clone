import CreateIcon from '@material-ui/icons/Create';
import React, { useState } from 'react';
import './feed.css';
import InputOption from './InputOption';
import PhotoIcon from '@material-ui/icons/Photo';
import VideocamIcon from '@material-ui/icons/Videocam';
import TodayIcon from '@material-ui/icons/Today';
import DescriptionIcon from '@material-ui/icons/Description';
import Post from './Post';
import { useEffect } from 'react';
import { db } from './Firebase';
import firebase from 'firebase';
function Feed() {
     
    const [input,setInput] = useState('');

    const [posts,setPost] = useState([]);

    useEffect(()=>{
        db.collection("posts").onSnapshot((snapshot)=>
        setPost(
            snapshot.docs.map((doc)=>(
                {
                    id: doc.id,
                    data: doc.data()
                }
            ))
        )
        );
    },[]);

   const sendPost = (e) => {
     e.preventDefault();
    
     db.collection('posts').add({
        name: 'Nasruddin',
        description: "Programmer at Authentic Venture Sdn Bhd",
        message: input,
        photoUrl:'',
        timestamp:firebase.firestore.FieldValue.serverTimestamp()

     });

   };

    return (
        <div className="feed">
            <div className="feed-inputContainer">
                <div className="feed-input">
                    <CreateIcon />
                    <form>
                        <input value={input} onChange={e=>setInput(e.target.value)} type="text" />
                        <button onClick={sendPost} type="submit">Send</button>
                    </form>
                </div>
                <div className="feed-inputOptions">
                 <InputOption icons={PhotoIcon} title="Photo" color="blue" />
                 <InputOption icons={VideocamIcon} title="Video" color="green" />
                 <InputOption icons={TodayIcon} title="Event" color="orange" />
                 <InputOption icons={DescriptionIcon} title="Write Article" color="red" />
            </div>
            </div>

            {/* post */}
             {posts.map(({id,data:{name,description,message,photoUrl}})=>(
                 <Post
                 key={id}
                 name={name}
                 description = {description}
                 message = {message}
                 photoUrl = {photoUrl}
                 
                 />
             ))}

          
           
        </div>
      
    )
}

export default Feed
