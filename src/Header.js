import React from 'react';
import "./header.css";
import SearchIcon from '@material-ui/icons/Search';
import HeaderOption from './HeaderOption';
import HomeIcon from '@material-ui/icons/Home';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import ChatIcon from '@material-ui/icons/Chat';
import NotificationsIcon from '@material-ui/icons/Notifications';


function Header() {
    return (
        <div className='header'>
            <div className="header-left">
                <img src="https://image.flaticon.com/icons/png/512/174/174857.png" alt="" />
                <div className="header-search">

                    <SearchIcon />
                    <input type="text" />
                </div>
            </div>
            <div className="header-right">
                <HeaderOption icons={HomeIcon}  title="Home"/>
                <HeaderOption icons={SupervisorAccountIcon} title="My Network"/>
                <HeaderOption icons={BusinessCenterIcon} title="Jobs"/>
                <HeaderOption icons={ChatIcon} title="Messaging"/>
                <HeaderOption icons={NotificationsIcon} title="Notifications"/>
                <HeaderOption avatar="https://media-exp1.licdn.com/dms/image/C4D03AQGBSgcJFTbR7A/profile-displayphoto-shrink_200_200/0/1618355904704?e=1632355200&v=beta&t=ftiOeTALSvcStBrg3hf6U3rsojSMaxN2cKA2AM-2KUY" title="me"/>
            </div>
        </div>
    )
}

export default Header
