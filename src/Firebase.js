import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyCedF3Hq9MG8AO4ffO2F3Dx8ZR5JcsnZu4",
    authDomain: "linkedin-clone-4edc4.firebaseapp.com",
    projectId: "linkedin-clone-4edc4",
    storageBucket: "linkedin-clone-4edc4.appspot.com",
    messagingSenderId: "928911098753",
    appId: "1:928911098753:web:8d0ab578a154e508045e80"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);

  const db = firebaseApp.firestore();
  const auth = firebase.auth();

  export {db,auth}
  